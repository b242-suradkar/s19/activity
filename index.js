let username, password, role;
function login(){
	
	username = prompt("enter your username:");
	password = prompt("enter your password:");
	role = prompt("enter your role:").toLowerCase();

	if(username === "" || password === "" || role ===""){
		alert("no empty");
	}
	else{
		switch(role){
			case "admin" :
				console.log("Welcome back to the class portal, admin!");
				break;
			case "teacher" :
				console.log("Thank you for logging in, teacher!");
				break;
			case "rookie" :
				console.log("Welcome to the class portal, student!");
				break;
			default :
				console.log("Role out of range.")
		}
	}
}
login();


function grade(one,two,three,four){
	let avg = (one+two+three+four)/4;
	avg = Math.round(avg);
	console.log(avg);

	if(avg<=74){
		console.log("Hello, student, your average is " + avg+". The letter equivalent is F");
	}
	else if(avg>=75 && avg<=79){
		console.log("Hello, student, your average is " + avg+". The letter equivalent is D");
	}
	else if(avg>=80 && avg<=84){
		console.log("Hello, student, your average is " + avg+". The letter equivalent is C");
	}
	else if(avg>=85 && avg<=89){
		console.log("Hello, student, your average is " + avg+". The letter equivalent is B");
	}
	else if(avg>=90 && avg<=95){
		console.log("Hello, student, your average is " + avg+". The letter equivalent is A");
	}
	else{
		console.log("Hello, student, your average is " + avg+". The letter equivalent is A+");
	}
}
grade(100,99,99,100);